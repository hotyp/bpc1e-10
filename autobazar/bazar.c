#include <stdio.h>
#include <string.h>
#include "bazar.h"


int ini_bazar(T_car **rec, int num)			// function for store initialization by chosen goods
{
    T_car  *new_car;

    //place part for reading data about all cars from bazar.txt file

    new_car = (T_car*)malloc(sizeof(T_car));
    strcpy(new_car->type, "Skoda Felicia");
    strcpy(new_car->color, "Blue");
    new_car->mot = 'D';
    new_car->vol = 1400;
    new_car->year = 2003;
    new_car->price = 75000;
    rec[num++] = new_car;

    new_car = (T_car*)malloc(sizeof(T_car));
    strcpy(new_car->type, "Skoda Felicia");
    strcpy(new_car->color, "Red");
    new_car->mot = 'D';
    new_car->vol = 1400;
    new_car->year = 2001;
    new_car->price = 55000;
    rec[num++] = new_car;

    new_car = (T_car*)malloc(sizeof(T_car));
    strcpy(new_car->type, "Alfa Romeo156");
    strcpy(new_car->color, "Black");
    new_car->mot = 'D';
    new_car->vol = 2000;
    new_car->year = 2004;
    new_car->price = 72000;
    rec[num++] = new_car;

    new_car = (T_car*)malloc(sizeof(T_car));
    strcpy(new_car->type, "VW Passat");
    strcpy(new_car->color, "Green");
    new_car->mot = 'B';
    new_car->vol = 1900;
    new_car->year = 2007;
    new_car->price = 123000;
    rec[num++] = new_car;

    new_car = (T_car*)malloc(sizeof(T_car));
    strcpy(new_car->type, "Skoda Octavia");
    strcpy(new_car->color, "White");
    new_car->mot = 'D';
    new_car->vol = 1900;
    new_car->year = 2004;
    new_car->price = 123500;
    rec[num++] = new_car;

    new_car = (T_car*)malloc(sizeof(T_car));
    strcpy(new_car->type, "Trabant S601");
    strcpy(new_car->color, "Green");
    new_car->mot = 'B';
    new_car->vol = 650;
    new_car->year = 1985;
    new_car->price = 6000;
    rec[num++] = new_car;

    new_car = (T_car*)malloc(sizeof(T_car));
    strcpy(new_car->type, "Fiat Uno");
    strcpy(new_car->color, "Blue");
    new_car->mot = 'B';
    new_car->vol = 1000;
    new_car->year = 1996;
    new_car->price = 25000;
    rec[num++] = new_car;

    new_car = (T_car*)malloc(sizeof(T_car));
    strcpy(new_car->type, "Porshe 911");
    strcpy(new_car->color, "Gold");
    new_car->mot = 'B';
    new_car->vol = 3500;
    new_car->year = 1995;
    new_car->price = 625000;
    rec[num++] = new_car;

    new_car = (T_car*)malloc(sizeof(T_car));
    strcpy(new_car->type, "Skoda Felicia");
    strcpy(new_car->color, "White");
    new_car->mot = 'D';
    new_car->vol = 1400;
    new_car->year = 2000;
    new_car->price = 48000;
    rec[num++] = new_car;

    new_car = (T_car*)malloc(sizeof(T_car));
    strcpy(new_car->type, "Honda Civic");
    strcpy(new_car->color, "Blue");
    new_car->mot = 'B';
    new_car->vol = 1500;
    new_car->year = 1993;
    new_car->price = 45000;
    rec[num++] = new_car;

    new_car = (T_car*)malloc(sizeof(T_car));
    strcpy(new_car->type, "Skoda Felicia");
    strcpy(new_car->color, "Red");
    new_car->mot = 'B';
    new_car->vol = 1100;
    new_car->year = 2000;
    new_car->price = 64000;
    rec[num++] = new_car;

    new_car = (T_car*)malloc(sizeof(T_car));
    strcpy(new_car->type, "Skoda Favorit");
    strcpy(new_car->color, "Black");
    new_car->mot = 'B';
    new_car->vol = 1300;
    new_car->year = 1993;
    new_car->price = 25000;
    rec[num++] = new_car;

    new_car = (T_car*)malloc(sizeof(T_car));
    strcpy(new_car->type, "Seat Leon");
    strcpy(new_car->color, "Black");
    new_car->mot = 'D';
    new_car->vol = 1900;
    new_car->year = 2008;
    new_car->price = 145000;
    rec[num++] = new_car;

    new_car = (T_car*)malloc(sizeof(T_car));
    strcpy(new_car->type, "Skoda Felicia");
    strcpy(new_car->color, "White");
    new_car->mot = 'D';
    new_car->vol = 1400;
    new_car->year = 2005;
    new_car->price = 92000;
    rec[num++] = new_car;

    return num;
}

void print_record(T_car *prcar)
{
    printf("\t%s\t %s", prcar->type, prcar->color);
    if(prcar->mot=='D')
        printf("\t diesel");
    else if(prcar->mot=='B')
        printf("\t beznin");
    printf("\t %4d ccm   year %d   price %6d CZK", prcar->vol, prcar->year, prcar->price);
}



void print_bazar(T_car **rec, int num)
{
    int n;

    for(n=0; n<num; n++)
    {
        printf("\n%2.0d  ", n+1);
        print_record(rec[n]);


    }

}

void erase_bazar(T_car **rec, int num)
{

    while(num>=0)      		// if at least two cars
    {
        free(rec[num]);
        num--;

    }
    printf("\nNa shledanou!\n");
}



void print_cars_price(T_car **rec, int num, int lo, int hi)
{
    int n;

    for(n=0; n<num; n++)
        if((rec[n]->price >= lo) && (rec[n]->price <= hi))
            print_record(rec[n]);
}


void print_cars_selected(T_car **rec, int num, int pr_lo, int pr_hi, char mt, int yr_lo, int yr_hi, int vol_lo, int vol_hi)
{
    int n;

    for(n=0; n<num; n++)
        if((rec[n]->price >= pr_lo) && (rec[n]->price <= pr_hi) &&
                (rec[n]->mot == mt) &&
                (rec[n]->year >= yr_lo) && (rec[n]->year <= yr_hi) &&
                (rec[n]->vol >= vol_lo) && (rec[n]->vol <= vol_hi))
            print_record(rec[n]);
}

int add_car(T_car **rec, int num)
{
    T_car   *new_car;
    char    str[20], ch;
    int     val;

    new_car = (T_car*)malloc(sizeof(T_car));
    printf("\nNew car \nInsert type: ");
    gets(str);
    fflush(stdin);
    strcpy(new_car->type, str);
    printf("\nInsert color: ");
    gets(str);
    fflush(stdin);
    strcpy(new_car->color, str);
    printf("\nInsert motor: ");
    scanf("%c", &ch);
    fflush(stdin);
    new_car->mot = ch;
    printf("\nInsert volume: ");
    scanf("%d", &val);
    fflush(stdin);
    new_car->vol = val;
    printf("\nInsert year: ");
    scanf("%d", &val);
    fflush(stdin);
    new_car->year = val;
    printf("\nInsert price: ");
    scanf("%d", &val);
    fflush(stdin);
    new_car->price = val;
    rec[num++] = new_car;

    return num;
}

int database_load(T_car **rec, int a)
{
    int i, num=0;
    T_car *new_car;
    FILE *fr;

    if((fr = fopen("bazar.txt", "rt")) != NULL)
    {
        printf("Databaze nalezena, nacitam auta z databaze\n");
        fscanf(fr, "%d\n", &num);

        for (i=0; i<num; i++)
        {
            new_car = (T_car*)malloc(sizeof(T_car));
            fgets(new_car->type, 19, fr);
            *(strchr(new_car->type, '\n')) = '\0';
            fscanf(fr, "%s\n", &new_car->color);
            fscanf(fr, "%c\n", &new_car->mot);
            fscanf(fr, "%d\n", &new_car->vol);
            fscanf(fr, "%d\n", &new_car->year);
            fscanf(fr, "%d\n", &new_car->price);
            rec[a++] = new_car;
        }

        fclose(fr);
        printf("Nalezeno %d aut.", num);
    }
    else
    {
        printf("Error, cannot open file.\n");
    }

    return a;
}

void database_store(T_car **rec, int num)
{
    int i,d,pocet=0;
    FILE *fw;

    if ((fw = fopen("bazar.txt", "wt")) != NULL)
    {

        for(d=0; d<num; d++)
        {
            if ((strcmp(rec[d]->type, "PRODANO") != 0))
            {
                pocet++;
            }
        }

        fprintf(fw, "%d", pocet);

        for (i=0; i<num; i++)
        {
            if ((strcmp(rec[i]->type, "PRODANO") != 0))
            {
                fprintf(fw, "\n%s", rec[i]->type);
                fprintf(fw, "\n%s", rec[i]->color);
                fprintf(fw, "\n%c", rec[i]->mot);
                fprintf(fw, "\n%d", rec[i]->vol);
                fprintf(fw, "\n%d", rec[i]->year);
                fprintf(fw, "\n%d", rec[i]->price);
                printf("\nCekejte prosim ukladam... auto %d z %d ", i+1, num);
            }
            else
            {
                printf("\nAuto cislo %d bylo prodano", i+1);
            }
        }

        fclose(fw);
        printf("\nUlozeno.\n");
    }
    else
    {
        printf("Error, cannot open file.\n");
    }

    return num;
}

void delete_car(T_car **rec, int num)
{
    int i=0;
    printf("\n\nVloz cislo auta ktere chces prodat: ");
    scanf("%d",&i);
    printf("\nMazu auto: %s",rec[i-1]->type);
    strcpy(rec[i-1]->type, "PRODANO");
}
