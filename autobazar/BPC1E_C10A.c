#include <stdio.h>
#include "bazar.h"

int main(void)
{
    T_car   *bazar[200];     // ptrs. to cars
    int     cnt=0;           // number of recorded cars
    int     lprice, hprice, lyear, hyear, lvol, hvol;
    char    cmd, tmot;

    cnt = database_load(bazar, cnt);
    if (cnt==0)
    {
        printf("Databaze nenalezena, nacitam defaultni sadu aut\n");
        cnt = ini_bazar(bazar, cnt);
    }
    printf("\n\nInsert command: 'q' = quit, 'p' = print all cars in bazar, 'a' = add new car,\n 's' = print selected cars 'd' - save bazar 'e' - prodat a smazat\n\n");
    scanf("%c", &cmd);
    fflush(stdin);
    while (cmd != 'q')						// if not quit
    {
        switch (cmd)
        {
        case 'p':
            print_bazar(bazar, cnt);
            break;
        case 'a':
            cnt=add_car(bazar, cnt);
            break;
        case 'z':
            erase_bazar(bazar, cnt);
            break;
        case 'e':
        {
            print_bazar(bazar, cnt);
            delete_car(bazar, cnt);
        }
        break;

        case 'd':
            database_store(bazar, cnt);
            break;
        case 's':
        {
            printf("\nSelect low limit of price:");
            scanf("%d", &lprice);
            fflush(stdin);
            printf("\nSelect high limit of price:");
            scanf("%d", &hprice);
            fflush(stdin);
            printf("\nSelect type of motor (B/D):");
            scanf("%c", &tmot);
            fflush(stdin);
            printf("\nSelect low limit of production year:");
            scanf("%d", &lyear);
            fflush(stdin);
            printf("\nSelect high limit of production year:");
            scanf("%d", &hyear);
            fflush(stdin);
            printf("\nSelect low limit of volume:");
            scanf("%d", &lvol);
            fflush(stdin);
            printf("\nSelect high limit of volume:");
            scanf("%d", &hvol);
            fflush(stdin);
            //print_cars_price(bazar, cnt, lprice, hprice);
            print_cars_selected(bazar, cnt, lprice, hprice, tmot, lyear, hyear, lvol, hvol);
        }
        }
        printf("\n\nInsert command: 'q' = quit, 'p' = print all cars in bazar, 'a' = add new car,\n 's' = print selected cars 'd' - save bazar 'e' - prodat a smazat\n\n");
        fflush(stdin);
        scanf("%c", &cmd);
        fflush(stdin);
    };
    database_store(bazar, cnt);
    erase_bazar(bazar, cnt); // deleting of all recods
    return 0;
}
