#ifndef BAZAR_H_INCLUDED
#define BAZAR_H_INCLUDED

typedef struct car     // car record
{
  char  type[20];           // type of car
  char  color[10];          // color of car
  char  mot;                // kind of the motor B means benzin, D means diesel
  int   vol;                // motor volume in cubic cm
  int   year;               // year of production
  int   price;              // price
} T_car;

int ini_bazar(T_car **rec, int num);			// function for bazar initialization by few cars
void print_record(T_car *prcar);                // function for printing of paramaters of chosen car
void print_bazar(T_car **rec, int num);         // function for printing of bazar content
void erase_bazar(T_car **rec, int num);         // function for deleting of all records (cars)
void print_cars_price(T_car **rec, int num, int lo, int hi);   // function for printing of bazar content with defined range of price
void print_cars_selected(T_car **rec, int num, int pr_lo, int pr_hi, char mt, int yr_lo, int yr_hi, int vol_lo, int vol_hi); // cars for selected ranges of params.
int add_car(T_car **rec, int num);              // function for adding of new car
int database_load(T_car **rec, int a);
void database_store(T_car **rec, int num);
void delete_car(T_car **rec, int num);

#endif // BAZAR_H_INCLUDED
